FROM debian:10 as build
RUN apt update && apt install -y wget gcc make libpcre++-dev zlib1g-dev


RUN wget https://nginx.org/download/nginx-1.19.3.tar.gz \
    && tar xvfz nginx-1.19.3.tar.gz \
    && cd nginx-1.19.3 \
    && ./configure \
    && make \
    && make install


FROM debian:10
WORKDIR /usr/local/nginx/sbin
RUN mkdir -p /usr/local/nginx/conf
COPY --from=build /usr/local/nginx/sbin/nginx .
COPY --from=build /usr/local/nginx/conf/nginx.conf /usr/local/nginx/conf/nginx.conf
COPY --from=build /usr/local/nginx/conf/mime.types /usr/local/nginx/conf/mime.types
RUN mkdir ../logs  && touch ../logs/error.log && chmod +x nginx

CMD ["./nginx", "-g", "daemon off;"]